package mx.edu.utel.poo.herencia;

public class Mascota {
    
    private String nombre;
    private int edad;
    private String raza;
    private String color;
    private Talla talla;
    
    public Mascota(String nombre, int edad, String raza, String color, Talla talla) {
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.color = color;
        this.talla = talla;
    }
       
    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getRaza() {
        return raza;
    }

    public String getColor() {
        return color;
    }

    public Talla getTalla() {
        return talla;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setTalla(Talla talla) {
        this.talla = talla;
    }

    @Override
    public String toString() {
        return String.format("Mascota: %s, %s, %d años, color: %s, tamaño: %s", 
                nombre, raza, edad, color, talla);
    }
    
    
    
    
}
