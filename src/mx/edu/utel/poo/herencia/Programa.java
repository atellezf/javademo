package mx.edu.utel.poo.herencia;

public class Programa {
    
    public static void main(String[] args) {
        Mascota m1 = new Mascota("Fido", 3, "Streeter", "Amarillo", Talla.GRANDE);
        System.out.println(m1);
        
        Perro p = new Perro("Solovino", 2, "Labrador", "Café", Talla.GRANDE, true);
        System.out.println(p);
        
        Mascota m2 = new Perro("Scooby", 4, "Gran Danés", "Café", Talla.EXTRAGRANDE, false);
        
        // Perro x = new Mascota("Benito Bodoque", 5, "Streeter", "Azul", Talla.CHICO);
        
        System.out.println(m2);
        // m2.setLadra(true);
        
        if(m2 instanceof Perro) {
            Perro p1 = (Perro) m2;
            p1.setLadra(true);
            System.out.println(p1);
        }
        
        
    }
    
    
}
