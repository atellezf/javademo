package mx.edu.utel.poo.herencia;

public class Perro extends Mascota {
    
    private boolean ladra;
    
    public Perro(String nombre, int edad, String raza, String color, Talla talla, boolean ladra) {
        super(nombre, edad, raza, color, talla);
        this.ladra = ladra;
    }

    public void setLadra(boolean ladra) {
        this.ladra = ladra;
    }

    public boolean isLadra() {
        return ladra;
    }

    @Override
    public String toString() {
        String cadena = ladra ? "este perro sí ladra" : "este perro no ladra";
        return String.format("Perro: %s, %s", super.toString(), cadena);
    }
    
    
    
    
}
