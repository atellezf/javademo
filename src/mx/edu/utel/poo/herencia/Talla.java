
package mx.edu.utel.poo.herencia;

public enum Talla {
    EXTRAGRANDE, GRANDE, MEDIANO, CHICO;
}
