package mx.edu.utel.poo.expresiones;

public class PruebaIF {

    public static void main(String[] args) {
        PruebaIF prueba1 = new PruebaIF();
        prueba1.condicionalSimple(18);
        prueba1.condicionalComplejo(13);
    }

    private void condicionalSimple(int edad) {
        if (edad < 18) {
            System.out.printf("Eres menor de edad, porque tienes %d años.\n", edad);
        } else {
            System.out.println("Eres mayor de edad");
        }
    }

    private void condicionalComplejo(int edad) {
        if (edad < 18) {
            if (edad < 12) {
                System.out.println("Eres un niño(a)");
            } else {
                System.out.println("Eres un adolescente");
            }
        } else {
            System.out.println("Eres mayor de edad");
        }
    }

}
