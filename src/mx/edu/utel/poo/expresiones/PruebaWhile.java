package mx.edu.utel.poo.expresiones;

public class PruebaWhile {
   
    public static void main(String[] args) {
        PruebaWhile prueba = new PruebaWhile();
//        prueba.imprimeSecuencia(20);
        prueba.imprimeSecuencia(1);
    }
    
    private void imprimeSecuencia(int x) {
        while (x<10) {
            System.out.println("Esto se imprime diez veces: " + (++x));
//            x++;
        }
        
        
    }
    
}
