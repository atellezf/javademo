/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utel.poo.expresiones;

/**
 *
 * @author alejandro
 */
public class PruebaDoWhile {

    public static void main(String[] args) {
        PruebaDoWhile prueba = new PruebaDoWhile();
//        prueba.pruebaCiclo(20);
        prueba.pruebaCiclo(1);
    }

    private void pruebaCiclo(int x) {
        do {
            System.out.println("Esto se imprime por lo menos una vez");
            x++;
        } while (x < 10);
    }

}
