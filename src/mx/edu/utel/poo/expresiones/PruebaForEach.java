package mx.edu.utel.poo.expresiones;

import java.util.ArrayList;
import java.util.List;

public class PruebaForEach {
    
    public static void main(String ... args) {
        PruebaForEach p = new PruebaForEach();

        // EN EL JDK 6 y anteriores
//        List<String> lista = new ArrayList<String>();
//        lista.add("Alexis");
//        lista.add("María del Pilar");
//        lista.add("Héctor");
//        p.imprimeLista(lista);
        
        // EN EL JDK 7
//        List<String> lista2 = new ArrayList<>();
//        lista2.add("Alexis");
//        lista2.add("María del Pilar");
//        lista2.add("Héctor");
//        p.imprimeLista(lista2);

        // EN EL JDK 8+
//        List<String> lista3 = List.of("Alexis","María del Pilar", "Héctor", "Leonardo");
//        p.imprimeLista(lista3);
        
        // EN EL JDK 10+
        var lista4 = List.of("Alexis","María del Pilar", "Héctor", "Leonardo");
        p.imprimeLista(lista4);
        
        int valor = 5;
        
        var valor2 = 5; // Es una variable entera
        var cadena = "Buenos días"; // Es una variable de cadena
        
        
    }
    
    private void imprimeLista(List<String> nombres) {
        // Programación funcional
        // nombres.forEach(System.out::println);
        for (String cad : nombres) {
            System.out.println("Alumno: " + cad);
            System.out.println("=============================");
        }
        
    }
    
}
