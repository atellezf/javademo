/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utel.poo.expresiones;

/**
 *
 * @author alejandro
 */
public class PruebaSwitch {
    
    public static void main(String[] args) {
        PruebaSwitch p = new PruebaSwitch();
        p.estructura(2);
    }
    
    private void estructura(int x) {
        switch (x) {
            case 2:
                System.out.println("Es el modelo 2002");
//                break;
            case 3:
                System.out.println("Es el modelo 2003");
                break;
            case 4:
                System.out.println("Es el modelo 2004");
                break;
            default:
                System.out.println("No se conoce el modelo");
        }
    }
            
    
}
