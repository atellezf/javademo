package mx.edu.utel.poo.excepciones;

public class ExcepcionAritmetica extends Exception {

    public ExcepcionAritmetica(String mensaje) {
        super(mensaje);
    }
    
}
