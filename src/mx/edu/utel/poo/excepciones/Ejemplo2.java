package mx.edu.utel.poo.excepciones;

public class Ejemplo2 {
    
    public static void main(String[] args) {
        Ejemplo2 e2 = new Ejemplo2();
        int x = e2.calcular();
        System.out.printf("El resultado fue: %d\n", x);
    }

    private int calcular() {
        try {
            int num = 12/2;
            System.out.println(num);
            System.exit(0); // Es la única que puede evitar el bloque finallly
            return num;
        } catch (ArithmeticException e) {
            System.out.println("No se puede dividir entre cero");
            return -1;
        } finally {
            System.out.println("Este bloque siempre se ejecutará.");
        }
    }
    
}
