package mx.edu.utel.poo.excepciones;

public class Ejemplo4 {

    public int calcular(int x, int y) throws ExcepcionAritmetica {
        return dividir(x, y);
    }

    public static void main(String[] args) {
        try {
            Ejemplo4 e4 = new Ejemplo4();
            int total = e4.calcular(2, 0);
            System.out.println(total);
        } catch (ExcepcionAritmetica e) {
            System.out.println(e.getMessage());
        }

    }

    private int dividir(int x, int y) throws ExcepcionAritmetica {
        return operacion(x, y);
    }

    /**
     * Este método realiza la operación deseada
     *
     * @param x Primer valor
     * @param y Segundo valor
     * @return El resultado de la división
     * @throws ArithmeticException En caso de una división entre cero
     */
    private int operacion(int x, int y) throws ExcepcionAritmetica {
        try {
            return x / y;
        } catch (ArithmeticException e) {
            var ex = new ExcepcionAritmetica("División entre cero");
            throw ex;
        }
        
    }

}
