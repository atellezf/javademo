package mx.edu.utel.poo.excepciones;

import java.util.Scanner;

public class Ejemplo1 {

    public static void main(String[] args) {
        Ejemplo1 e1 = new Ejemplo1();
        e1.solicitarDatoConExcepciones();
//        e1.solicitarDato();
    }

    private void solicitarDato() {
        // Anticiparse a la Excepción es lo correcto
        Scanner scn = new Scanner(System.in);
        System.out.print("Ingresa un valor numérico: ");
        int val = scn.nextInt();
        if(val > 0) {
            int x = 100;
            double result = x / val;
            System.out.printf("La división es: %f\n", result);
        } else {
            System.out.println("Debe ser un número mayor que 0.");
        }
    }

    private void solicitarDatoConExcepciones() {
        // Sintácticamente correcto pero no es correcto en el flujo natural.
        try {
            Scanner scn = new Scanner(System.in);
            System.out.print("Ingresa un valor numérico: ");
            int val = scn.nextInt();
            int x = 100;
            double result = x / val;
            System.out.printf("La división es: %f\n", result);
        } catch (ArithmeticException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

}
