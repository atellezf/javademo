package mx.edu.utel.poo.excepciones;

public class Ejemplo3 {

    public int calcular(int x, int y) throws ArithmeticException {
        return dividir(x, y);
    }

    public static void main(String[] args) {
        try {
            Ejemplo3 e3 = new Ejemplo3();
            int total = e3.calcular(2, 0);
            System.out.println(total);
        } catch (ArithmeticException e) {
            System.out.println("No es posible dividir entre cero");
            e.printStackTrace();
        }

    }

    private int dividir(int x, int y) throws ArithmeticException {
        return operacion(x, y);
    }

    /**
     * Este método realiza la operación deseada
     *
     * @param x Primer valor
     * @param y Segundo valor
     * @return El resultado de la división
     * @throws ArithmeticException En caso de una división entre cero
     */
    private int operacion(int x, int y) throws ArithmeticException {
        return x / y;
    }

}
