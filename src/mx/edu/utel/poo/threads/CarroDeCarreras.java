package mx.edu.utel.poo.threads;

public class CarroDeCarreras extends Thread {
    
    private final String piloto;
    private final int numero;
    private static int lugar;

    public CarroDeCarreras(String piloto, int numero) {
        this.piloto = piloto;
        this.numero = numero;
    }

    @Override
    public String toString() {
        return String.format("Carro: #%d, Piloto: %s", numero, piloto);
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                System.out.printf("El auto #%d ha recorrido %d millas.\n", numero, i);
                Thread.sleep(1000);
            }
            System.out.printf("%s llego en %d lugar.\n", piloto, obtenerLugar());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private synchronized int obtenerLugar() {
        return ++lugar;
    }
    
    
    
}
