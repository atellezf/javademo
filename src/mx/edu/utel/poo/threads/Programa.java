
package mx.edu.utel.poo.threads;

import java.util.ArrayList;

public class Programa {
    
    public static void main(String[] args) {
        var lista = new ArrayList<CarroDeCarreras>();
        lista.add(new CarroDeCarreras("Benito Bodoque", 1));
        lista.add(new CarroDeCarreras("Don Gato", 2));
        lista.add(new CarroDeCarreras("Demóstenes", 3));
        lista.add(new CarroDeCarreras("Matute", 4));
        lista.add(new CarroDeCarreras("Cucho", 5));
        for (CarroDeCarreras carro : lista) {
            carro.start();
        }
        
    }
    
    
}
